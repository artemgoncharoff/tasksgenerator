import Vue from 'vue'
import App from './App.vue'
import {store} from "./store";
import VueLogger from 'vuejs-logger'

Vue.config.productionTip = false;

const loggerOptions = {
  isEnabled: true,
  logLevel : process.env.VUE_APP_DEVELOPMENT ? 'debug' : 'error',
  stringifyArguments : false,
  showLogLevel : true,
  showMethodName : true,
  separator: '|',
  showConsoleColors: true
}

//@ts-ignore
Vue.use(VueLogger, loggerOptions);

//@ts-ignore
new Vue({
  //@ts-ignore
  store,
  render: h => h(App),
}).$mount('#app');
