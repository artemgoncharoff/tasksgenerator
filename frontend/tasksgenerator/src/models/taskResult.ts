/**
 * Результат решения конкретной задачи
 * todo: использовать taskId для корректной работы, даже если локальный порядок задач отличается от порядка в ответе
 */
export interface TaskResult {
    taskId: number,
    is_correct: boolean,
}