/**
 * «Заказ» на количество задач типа
 */
export interface TaskTypeOrder {
    typeId: number,
    num: number,
}