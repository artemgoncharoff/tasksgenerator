import {NumberConfig} from "@/models/numberConfig";

/**
 * Конфигурация работы приложения
 */
export default interface Config {
    /**
     * Допустимые уровни сложности
     */
    difficulty: NumberConfig;

    /**
     * Допустимое количество вариантов
     */
    variantsNumber: NumberConfig;
}