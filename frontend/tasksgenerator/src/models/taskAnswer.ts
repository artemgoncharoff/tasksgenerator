/**
 * Ответ на задачу
 */
export interface TaskAnswer {
    taskId: number
    value: string[]
}