import {NumberConfig} from "@/models/numberConfig";
import Task from "@/models/task";

/**
 * «Возможность»
 */
export interface TaskOption {
    /**
     * Пример задачи типа.
     * hint: Тип передается внутри задачи
     */
    task: Task,

    /**
     * Количество возможных задач категории
     */
    available: NumberConfig,
}