import {TaskResult} from "@/models/taskResult";

/**
 * Результат решения варианта
 */
export interface Result {
    /**
     * Количество набранных баллов
     */
    score: number,

    /**
     * Правильные решения в порядке, в котором задачи были
     * отправлены на сервер.
     */
    result: TaskResult[]
}