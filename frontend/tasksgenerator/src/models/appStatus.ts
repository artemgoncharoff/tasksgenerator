export type AppStatus =
    { status: "Greetings", value: 0 } |
    { status: "SelectingParams", value: 1 } |
    { status: "SelectingTasks", value: 2 } |
    { status: "Solving", value: 3 } |
    { status: "Solved", value: 4 };

export const getAppStatus = (identifier) => {
    switch (identifier) {
        case 0:
        case "Greetings":
            return { status: "Greetings", value: 0 }

        case 1:
        case "SelectingParams":
            return { status: "SelectingParams", value: 1 }

        case 2:
        case "SelectingTasks":
            return { status: "SelectingTasks", value: 2 }

        case 3:
        case "Solving":
            return { status: "Solving", value: 3 }

        case 4:
        case "Solved":
            return { status: "Solved", value: 4 }
        
        default:
            return null;
    }
}

export const initialAppStatus = getAppStatus(0);