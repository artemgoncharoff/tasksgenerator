/**
 * Диапазон допустимых значений X:
 * min <= X <= max
 */
export interface NumberConfig {
    readonly min: number;
    readonly max: number;
}