import Task from "@/models/task";

/**
 * Вариант для решения
 */
export interface Variant {
    id: number,
    seed: String,
    tasks: Task[],
}