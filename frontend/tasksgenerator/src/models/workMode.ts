/**
 * Выбранный режим работы
 */
export default interface WorkMode {
    difficulty: Number;
    variantsNumber: Number;
}