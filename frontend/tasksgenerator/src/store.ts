import Vue from 'vue';
import Vuex, { StoreOptions } from 'vuex';
import WorkMode from "@/models/workMode";
import Config from "@/models/config";
import { createVariantAPI, getConfig, getTaskOptions, takeWork, seedVariant } from "@/api";
import { Variant } from "@/models/variant";
import { Result } from "@/models/result";
import { AppStatus, initialAppStatus, getAppStatus } from "./models/appStatus";
import { TaskAnswer } from "@/models/taskAnswer";
import { TaskOption } from "@/models/taskOption";
import { TaskTypeOrder } from "@/models/taskTypeOrder";

Vue.use(Vuex);
const { getField, updateField } = require('vuex-map-fields');

export let store: StoreOptions<{
    workMode: WorkMode,
    config: Config | null,
    ready: boolean,
    taskOptions: TaskOption[],
    taskTypeOrder: TaskTypeOrder[],
    variant: Variant | null,
    variants: Variant[] | null,
    taskAnswers: TaskAnswer[],
    result: Result | null,
    appStatus: AppStatus,
    seed: String,
}>;

store = new Vuex.Store(
    {
        state: {
            workMode: {
                difficulty: 1,
                variantsNumber: 1
            },

            config: {
                difficulty: { min: 0, max: 10 },
                variantsNumber: { min: 0, max: 10 },
            },

            taskOptions: [],
            ready: false,
            taskTypeOrder: [] as TaskTypeOrder[],
            taskAnswers: [] as TaskAnswer[],
            variant: null,
            variants: null,
            result: null,
            appStatus: initialAppStatus as AppStatus,
            seed: "" as String
        },

        getters: {
            getField,
            isTaskTypeOrdersReady: (state) => state.taskTypeOrder.some(x => x.num > 0),
            isAllVariantsSolved: (state) => { return (state.variants as unknown as Variant[]).length == 0; } //
        },

        mutations: {
            setReady(state, p) {
                state.ready = p;
                (Vue as any).$log.info('Ready set to ' + JSON.stringify(p));
            },

            setConfig(state, config: Config) {
                state.config = config;
                (Vue as any).$log.info('Config set to ' + JSON.stringify(config));
            },
            
            setTaskOptions(state, taskOptions: TaskOption[]) {
                Vue.set(state, 'taskOptions', [...taskOptions]);
                Vue.set(state, 'taskTypeOrder', [] as TaskTypeOrder[]);
                if (taskOptions != null) {
                    for (let i = 0; i < taskOptions.length; i++) {
                        state.taskTypeOrder.push({
                            typeId: taskOptions[i].task.id,
                            num: 0,
                        });
                    }
                    (Vue as any).$log.info('Task type order updated and set to ' + JSON.stringify(state.taskTypeOrder));
                }
                (Vue as any).$log.info('Task options set to ' + JSON.stringify(taskOptions));
            },

            setVariant(state, variant: Variant) {
                Vue.set(state, "variant", variant);
                Vue.set(state, 'taskAnswers', [] as TaskAnswer[]);
                if (variant != null && variant.tasks != null) {
                    for (let i = 0; i < variant.tasks.length; i++) {
                        state.taskAnswers.push({
                            taskId: variant.tasks[i].id,
                            value: [],
                        });
                    }
                    (Vue as any).$log.info('Task answers updated and set to ' + JSON.stringify(state.taskAnswers));
                }
                (Vue as any).$log.info('Variant set to ' + JSON.stringify(variant));
            },

            setVariants(state, variants: Variant[]) {
                Vue.set(state, "variants", variants);
                (Vue as any).$log.info('Variants set to ' + JSON.stringify(variants));
            },

            setResult(state, result: Result) {
                Vue.set(state, "result", result);
                (Vue as any).$log.info('Result set to ' + JSON.stringify(result));
            },

            setAppStatus(state, appStatus: AppStatus) {
                state.appStatus = appStatus;
                (Vue as any).$log.info('AppStatus set to ' + JSON.stringify(appStatus));
            },

            updateField
        },

        actions: {
            showCreateVariant({ state, dispatch, commit }) {
                commit("setReady", false);
                commit("setAppStatus", getAppStatus("SelectingParams"))
            },

            updateConfig({ commit }) {
                return getConfig().then((c) => {
                    commit("setConfig", c);
                    commit("setReady", true);
                    (Vue as any).$log.info('Update config complete');
                })
            },

            getTaskOptions() {
                return getTaskOptions();
            },

            showTaskOptions({ state, dispatch, commit }) {
                commit("setReady", false);
                return dispatch("getTaskOptions").then((s) => {
                    commit("setTaskOptions", s);
                    commit("setReady", true);
                    commit("setAppStatus", getAppStatus("SelectingTasks"));
                    (Vue as any).$log.info('Show task options complete');
                });
            },

            seedVariant({ state, commit }) {
                commit("setReady", false); //?
                return seedVariant(state.seed).then((variants: Variant[]) => {
                    commit("setVariants", variants);
                    commit("setVariant", variants.pop());
                    commit("setReady", true);
                    commit("setAppStatus", getAppStatus("Solving"));
                    (Vue as any).$log.info('Create varinant with seed complete');
                })
            },

            createVariant({ state, commit }) {
                commit("setReady", false);
                return createVariantAPI(state.workMode, state.taskTypeOrder).then((variants: Variant[]) => {
                    commit("setVariants", variants);
                    commit("setVariant", variants.pop());
                    commit("setReady", true);
                    commit("setAppStatus", getAppStatus("Solving"));
                    (Vue as any).$log.info('Create varinant complete');
                })
            },

            sendVariant({ state, dispatch, commit }) {
                commit("setReady", false);
                return takeWork(state.variant!, state.taskAnswers).then((result: Result) => {
                    commit("setResult", result);
                    commit("setReady", true);
                    commit("setAppStatus", getAppStatus("Solved"));
                    (Vue as any).$log.info('Variant send and take work complete');
                })
            },

            resetState({state, commit}) {
                commit("setReady", false);
                commit("setTaskOptions", []);
                commit("setVariants", null);
                commit("setVariant", null);
                commit("setResult", null);
                commit("setAppStatus", getAppStatus(1));
                (Vue as any).$log.info('Store state reset complete');
            },
            
            setGreetingsPage({state, commit}) {
                commit("setAppStatus", getAppStatus(0));
                (Vue as any).$log.info('Greetings page is set');
            },

            nextVariant({state, commit}) {
                if (state != null && state.variants != null){ 
                    let v = (state.variants as unknown as Variant[]).pop();
                    if (v !== undefined) {
                        commit("setAppStatus", getAppStatus('Solving'))
                        commit("setResult", null);
                        commit("setTaskOptions", []);
                        commit("setVariant", v);
                    }
                }
            },
        }
    }
);