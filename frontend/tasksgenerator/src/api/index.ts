import axios from 'axios'
import qs from 'qs'
import Config from "@/models/config";
import {Variant} from "@/models/variant";
import {TaskAnswer} from "@/models/taskAnswer";
import {TaskTypeOrder} from "@/models/taskTypeOrder";
import WorkMode from '@/models/workMode';
import Vue from 'vue';
import App from '../App.vue';
import { store } from '../store';

//let domain = process.env.VUE_APP_HOST;
//let domain = 'http://127.0.0.1:8000/' //for local run
let domain = 'http://tasksgenerator-env.eba-piuqmpga.us-east-1.elasticbeanstalk.com/' //for development server run

export const getDomain = () => {
    return domain
};

export const setDomain = ($domain: string) => {
    domain = $domain
};

const request = (method: "get" | "post", path: string, data: any = {}) => {
    let url = domain + 'api/' + path;

    if(method == "get") {
        if (Object.keys(data).length > 0) {
            url = url + '?' + qs.stringify(data)
        }
    }
    (Vue as any).$log.info('Request HTTP ' + method.toUpperCase() + ' to api/' + path + "" + (Object.keys(data).length == 0 ? ' with no payload' : ' with payload: ' + JSON.stringify(data)));
    return axios.request<{ status: "ok" | "fail", "response": any }>({
        method: method,
        url: url,
        data: data
    })
        .then(d => {
                const result = d.data;
                if (result.status === "ok")
                    return result.response;
                throw new Error("Bad api response: " + JSON.stringify(result));
            }
        ).catch(error => {
            (Vue as any).$log.error('Request HTTP ' + method.toUpperCase() + ' to api/' + path + ' failed! ' + (error as Error).message);
            const app = new App();
            (app as any).$notify.error({
                title: 'Ошибка на сервере!',
                message: 'Пожалуйста, попробуйте позже'
            });
        });
};

// ------- * API METHODS * -------

export const getConfig = (() => request("get", "config"));

export const getTaskOptions = (() => request("get", "available_tasks"));

export const seedVariant = ((seed: String) => 
    request("post", "seed_tasks", {
        seed: seed
    }));

export const createVariantAPI = ((wm: WorkMode, t: TaskTypeOrder[]) => 
    request("post", "selected_tasks", {
        work_mode: wm,
        task_option: t,
    }));

export const takeWork = ((v: Variant, answers: TaskAnswer[]) => 
    request("post", "verify_answers", {
        variantId: v.id,
        answers: answers,
    }));
