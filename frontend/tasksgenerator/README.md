# Tassker

Фронтенд часть приложения. Используются:

- TypeScript
- Vue
- Vuex
- Axios
- Vuex-map-fields
- Vue-html-to-paper
- Element UI


## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
