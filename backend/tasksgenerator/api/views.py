import hashlib
import logging

from django.http import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from . import constants
from .database import write_task, read_task, read_and_delete_task
import json
from ProblemGenerator import main as pg
import random


# get
@csrf_exempt
def config(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting configurations')

    if request.method == 'GET':
        response = JsonResponse({"status": "ok",
                                 "response": {"difficulty": {"max": constants.DIFFICULTY_MAX,
                                                             "min": constants.DIFFICULTY_MIN},
                                              "variantsNumber": {"max": constants.VARIANTS_NUMBER_MAX,
                                                                 "min": constants.VARIANTS_NUMBER_MIN}}})
        logging.info('Ended getting configurations')
        return response
    else:
        response = not_allowed(request)
        logging.error('Failed getting configurations')
        return response


#get
@csrf_exempt
def available_tasks(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting available tasks')
    if request.method == 'GET':
        examples = pg.get_examples()
        tasks = []
        for i in range(len(examples)):
            text, correct_answer, answer_template, header = examples[i]()
            task = {"available": {"min" : constants.TASK_NUMBER_MIN, "max": constants.TASK_NUMBER_MAX}, "task": {"id": i, "text": text, "header": header, "type": answer_template, "converters": ["tex"]}}
            tasks.append(task)
        logging.info('Ended getting available tasks')
        return JsonResponse({"status": "ok", "response": tasks})
    else:
        logging.error('Failed getting available tasks')
        return not_allowed(request)


# post
@csrf_exempt
def selected_tasks(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started selecting tasks')
    random.seed() #using currend time as random generator seed
    if request.method == 'POST':
        
        data = json.loads(request.body)
        difficulty = data["work_mode"]["difficulty"]
        variantsNumber = data["work_mode"]["variantsNumber"]
        seed = random.randint(0, 4611686018427) # seed
        seed *= 100
        seed += 10 * (difficulty - 1) + 1 * (variantsNumber - 1)
        seed *= 1000000
        i = 0;
        task_option = data["task_option"]
        for task in task_option:
            num = task["num"]
            seed += (10 ** i) * num
            i += 1
        print(seed)
        variants = generate_tasks(seed)
        
        logging.info('Ended selecting tasks')
        return JsonResponse({"status": "ok", "response": variants})
    else:
        response = not_allowed(request)
        logging.error('Failed selecting tasks')
        return response


# post
@csrf_exempt
def seed_tasks(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started seeding tasks')
    if request.method == 'POST':
        data = json.loads(request.body)

        seed = data["seed"]
        
        
        variants = generate_tasks(seed)

        logging.info('Ended seeding tasks')
        return JsonResponse({"status": "ok", "response": variants})
    else:
        response = not_allowed(request)
        logging.error('Failed seeding tasks')
        return response
    

def generate_tasks(seed):
    str_seed = str(seed)
    try:
        int_seed = int(seed)
    except ValueError:
        int_seed = hash(seed)

    if int_seed < 100000000:
        int_seed = 100000000 * int_seed + int_seed

    nums = []
    for i in range(6):
        nums.append(int_seed % 10)
        int_seed = int_seed // 10
    
    
    variants_number = int_seed % 10 + 1
    int_seed = int_seed // 10
    difficulty = int_seed % 10 + 1
    int_seed = int_seed // 10
    variants = []
    for j in range(variants_number):
        var_id = -1
        tasks = []
        for task in range(len(nums)):
            typeId = task
            num = nums[task]
            for i in range(num):
                text, correct_answer, answer_template, header = pg.tasks[typeId](int_seed + i + len(nums)*j, difficulty)
                taskId = write_task(correct_answer)
                if var_id == -1:
                    var_id = taskId
                task = {"available": {"min": constants.TASK_NUMBER_MIN, "max": constants.TASK_NUMBER_MAX},
                        "task": {"id": taskId, "text": text, "header": header, "type": answer_template,
                                    "converters": ["tex"]}}
                tasks.append(task)
        variant = {"id": var_id, "seed" : str_seed, "tasks": tasks}
        if var_id != -1:
            variants.append(variant)
    
    return variants

# post
@csrf_exempt
def verify_answers(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started verifying answers')

    if request.method == 'POST':
        data = json.loads(request.body)
        var_id = data["variantId"]
        answers = data["answers"]
        result = []
        num_of_correct = 0
        num_total = len(answers)
        for i in range(len(answers)):
            correct_answer = read_task(var_id + i)
            user_answer = answers[i]["value"]
            equal = user_answer == correct_answer
            result.append({"taskId": var_id + i, "is_correct": equal})
            if equal:
                num_of_correct += 1
        status = "ok"
        score = round(num_of_correct / num_total * 100)

        response = JsonResponse({"status": status, "response": {"score": score, "result": result}})
        logging.info('Ended verifying answers')
        return response
    else:
        response = not_allowed(request)
        logging.error('Failed verifying answers')
        return response


@csrf_exempt
def not_found(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started making not found response')

    # Error 404 Not found
    # see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10
    response = HttpResponse('')
    response.status_code = 404

    logging.info('Ended making not found response')
    return response


@csrf_exempt
def not_allowed(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started making not allowed response')

    # Error 405 Method Not Allowed
    # see https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10
    response = HttpResponse('')
    response.status_code = 405

    logging.info('Ended making not allowed response')
    return response


# database access tests
@csrf_exempt
def read(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started reading from database')

    task_id = request.GET['id']
    answers = read_task(task_id)

    response = JsonResponse({"status": "ok", "response": {"answers": answers}})
    logging.info('Ended reading from database')
    return response


@csrf_exempt
def read_delete(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started reading and delete from database')

    task_id = request.GET['id']
    answers = read_and_delete_task(task_id)

    response = JsonResponse({"status": "ok", "response": {"answers": answers}})
    logging.info('Ended reading and delete from database')
    return response


@csrf_exempt
def write(request):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started writing to database')

    arr = request.GET.copy().pop('q')
    task_id = write_task(arr)

    response = JsonResponse({"status": "ok", "response": {"answers": arr, "id": task_id}})
    logging.info('Ended writing to database')
    return response



