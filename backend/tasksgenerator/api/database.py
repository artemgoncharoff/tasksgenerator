from .models import Task

# array to string
def serialize_answers(answers):
    result = ''
    for i in range(len(answers)):
        result += answers[i].__str__()     #todo: float numbers
        if i + 1 < len(answers):
            result += ';'

    return result


# string to array
def deserialize_answers(text):
    answers = text.split(';')
    return answers


# writes correct_answer[] to database
def write_task(correct_answer):
    serialized = serialize_answers(correct_answer)
    task = Task(correct_answer = serialized)
    task.save()
    return task.pk


# reads correct_answer[] from database, if not found returns empty array
def read_task(task_id):
    try:
        task = Task.objects.get(pk = task_id)
        result = deserialize_answers(task.correct_answer)
        return result
    except Task.DoesNotExist:
        return []


# deletes record `task_id` in database, if success returns True, else False
def delete_task(task_id):
    try:
        task = Task.objects.get(pk = task_id)
        task.delete()
        return True
    except Task.DoesNotExist:
        return False


# reads and deletes correct_answer[] from database by `task_id`, if not found returns empty array
def read_and_delete_task(task_id):
    try:
        task = Task.objects.get(pk = task_id)
        result = deserialize_answers(task.correct_answer)
        task.delete()
        return result
    except Task.DoesNotExist:
        return []