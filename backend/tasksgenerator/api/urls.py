from django.urls import path
from . import views

urlpatterns = [
    path('config', views.config),
    path('available_tasks', views.available_tasks),
    path('selected_tasks', views.selected_tasks),
    path('seed_tasks', views.seed_tasks),
    path('verify_answers', views.verify_answers),
    # database tests
    # path('read', views.read),
    # path('write', views.write),
    # path('read_delete', views.read_delete),
    path('', views.not_found)

]