from django.db import models

# Create your models here.

class Task(models.Model):
    correct_answer = models.TextField(default="") # correct_answer is array (i.e. [123, 42, 576]) serialized to string (i.e. "123;42;576") with ; as a delimiter
