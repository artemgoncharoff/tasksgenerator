# Tasks Generator  

### Запуск
~~~
python backend/tasksgenerator/manage.py runserver
~~~

### Добавление библиотек в файл `requirements.txt`
*Только при использовании виртуального окружения!*
~~~
pip install lib_name
pip freeze > backend/tasksgenerator/requirements.txt
~~~

### Подключение всех библиотек из файла `requirements.txt`
~~~
pip install -r backend/tasksgenerator/requirements.txt
~~~
