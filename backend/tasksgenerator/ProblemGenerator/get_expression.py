# coding=utf-8
import logging
import math
import random
import operator

from fractions import Fraction


class Expression:
    operators = [Fraction.__add__, Fraction.__sub__, Fraction.__mul__, Fraction.__truediv__,
                 Fraction.__pow__, Fraction.__abs__]


# Генерация ответов к выражению
def get_correct_answer():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating correct answer')
    correct_answer = [random.randint(-10, 10)]

    logging.info('Ended generating correct answer')
    return correct_answer


# Возвращает массив имен полей ответа
def get_answer_template():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting answer template')

    answer_template = ['$$x$$']

    logging.info('Ended getting answer template')
    return answer_template


# Создает и возвращает выражение для вычисления
def make_expression(correct_answer, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started making expression')

    # Выражение в ОПН
    stack = ['x']

    # Число, к которому приравниваем выражение с х
    res = Fraction(correct_answer, 1)

    difficulty_local = 0
    prev_operation_index = -1

    # Чило возведений в степень
    pows = 0

    # Число модулей
    cnt_abs = 0

    while difficulty_local < difficulty:
        while True:
            # Запрещаем подряд одинаковые(быстроупрощаемые) операции(кроме возведения в степень)
            rand_operation_index = random.randint(0, len(Expression.operators) - 1)
            while prev_operation_index == rand_operation_index and prev_operation_index != 4 \
                    or prev_operation_index == 0 and rand_operation_index == 1 \
                    or prev_operation_index == 1 and rand_operation_index == 0 \
                    or prev_operation_index == 2 and rand_operation_index == 3 \
                    or prev_operation_index == 3 and rand_operation_index == 2 \
                    or prev_operation_index == 1 and rand_operation_index == 0:
                rand_operation_index = random.randint(0, len(Expression.operators) - 1)

            # Операции без модуля
            if rand_operation_index != 5 and rand_operation_index != 6:
                # Ставим разные ограничения на каждую операцию
                rand_operand = 0
                if rand_operation_index != 4:
                    while rand_operand == 0:
                        rand_operand = random.randint(-10, 10)
                else:
                    if pows > 2:
                        continue
                    while rand_operand == 0 or abs(rand_operand) == 1:
                        rand_operand = random.randint(-3, 3)
                    if res == 0 and rand_operand <= 0:
                        rand_operand = random.randint(1, 3)

                if rand_operation_index == 3 or rand_operation_index == 2:
                    while abs(rand_operand) == 1 or rand_operand == 0:
                        rand_operand = random.randint(-5, 5)

                operation = Expression.operators[rand_operation_index]
                ans_operation = operation(res, rand_operand)

                # Если после добавления операции получается большое число, то заново генерим операцию
                if abs(ans_operation.numerator) > 64 or abs(ans_operation.denominator) > 64:
                    continue

                if rand_operation_index == 4:
                    pows += 1

                stack.append(rand_operand)
                stack.append(operation)
                res = ans_operation
            # Операция Модуль
            elif rand_operation_index == 5:
                if cnt_abs < 3:
                    operation = Expression.operators[rand_operation_index]
                    stack.append(operation)
                    res = operation(res)
                    cnt_abs += 1
                else:
                    continue

            # Операция Корень
            '''elif rand_operation_index == 6:
                operation = Expression.operators[rand_operation_index]
                stack.append(operation)
                if res < 0:
                    difficulty_local += 1
                    stack.append(Expression.operators[5])
                    res = Expression.operators[5](res)
                res = Fraction(round(pow(res.numerator, 0.5)), round(pow(res.denominator, 0.5)))'''

            difficulty_local += 1
            prev_operation_index = rand_operation_index
            break

    logging.info('Ended making expression')
    return stack, res


# Возвращает представление выражения в LaTeX
def get_latex_string(expr, correct_answer):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started converting expression to LaTex')

    has_abs_or_odd_pow = False
    stack = expr
    n = len(stack)
    stack_nums = []
    expression = stack.pop(0).__str__()
    prev_operation = Fraction.__floor__
    n -= 1

    while n > 0:
        element = stack.pop(0)
        n -= 1

        if element == Fraction.__add__:
            b = stack_nums.pop(0)
            if b < 0:
                expression += '-' + (-b).__str__()
            else:
                expression += '+' + b.__str__()
            prev_operation = element

        elif element == Fraction.__sub__:
            b = stack_nums.pop(0)
            if b < 0:
                expression += '+' + (-b).__str__()
            else:
                expression += '-' + b.__str__()
            prev_operation = element

        elif element == Fraction.__mul__:
            b = stack_nums.pop(0)
            if (prev_operation == Fraction.__add__ or prev_operation == Fraction.__sub__) and len(expression) > 1:
                expression = '\\left( ' + expression + '\\right) '
            expression += '\\cdot '

            if b < 0:
                expression += '\\left( ' + b.__str__() + '\\right) '
            else:
                expression += b.__str__()
            prev_operation = element

        elif element == Fraction.__truediv__:
            b = stack_nums.pop(0)

            if (prev_operation == Fraction.__add__ or prev_operation == Fraction.__sub__) and len(expression) > 1:
                expression = '\\left( ' + expression + '\\right) '
            expression = '\\frac {' + expression + '}{'

            if b < 0:
                expression += '\\left( ' + b.__str__() + '\\right)} '
            else:
                expression += b.__str__() + '}'
            prev_operation = element

        elif element == Fraction.__pow__:
            b = stack_nums.pop(0)

            if len(expression) > 1:
                expression = '\\left( ' + expression + '\\right) '

            expression += '^{' + b.__str__() + '}'
            if abs(b) % 2 == 0:
                has_abs_or_odd_pow = True
            prev_operation = element

        elif element == Fraction.__abs__:
            expression = '\\left| ' + expression + '\\right| '
            has_abs_or_odd_pow = True
            prev_operation = element

        else:
            stack_nums.append(element)

    latex_string = '$' + expression

    logging.info('Ended converting linear system to LaTex')
    return latex_string, has_abs_or_odd_pow


# Возвращает пример задание (пример неизменяется)
def get_expression_example():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting example of expression')

    example = get_expression(0, 1)
    logging.info('Ended getting example of expression')
    return example


# Генерация выражения
def get_expression(seed, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting LaTex string, correct answer, answer template and header of expression')

    header = 'Вычисление значения выражения'
    random.seed(seed)
    correct_answer = get_correct_answer()[0]
    answer_template = get_answer_template()

    expr, res = make_expression(correct_answer, difficulty)
    latex_string, has_abs_or_odd_pow = get_latex_string(expr, correct_answer)
    latex_string += '= ' + res.__str__()
    if has_abs_or_odd_pow:
        if correct_answer < 0:
            latex_string += ', x \leq 0$'
        else:
            latex_string += ', x \geq 0$'
    else:
        latex_string += '$'

    logging.info('Ended getting LaTex string, correct answer, answer template and header of expression')
    return latex_string, [correct_answer], answer_template, header
    # return expr, res, correct_answer


