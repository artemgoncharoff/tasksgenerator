import logging
import random


# Возвращает уравнение одной неизвестно v1.0
def get_motion_problem(seed, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating and returning LaTex string, correct answer, '
                 'answer template and header for motion problem with difficulty level %s', difficulty)

    random.seed(seed)

    latex_string = ''
    correct_answer = [0]
    if difficulty <= 3:
        latex_string, correct_answer[0] = get_motion_problem_easy()
    if 3 < difficulty < 7:
        latex_string, correct_answer[0] = get_motion_problem_medium()
    if 6 < difficulty:
        latex_string, correct_answer[0] = get_motion_problem_difficult()

    header = 'Задача на движение'

    answer_template = ['']

    logging.info('Ended generating and returning LaTex string, correct answer, '
                 'answer template and header for motion problem with difficulty level %s', difficulty)
    return latex_string, correct_answer, answer_template, header

def get_motion_problem_easy():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating motion problem with 1 - 3 difficulty level')
    v = random.randint(1, 10)
    u = random.randint(10, 30)
    t = random.randint(2, 6)
    s = (v + u) * t
    latex_string = 'Из двух пунктов, расстояние между которыми ' + s.__str__() + ' км, одновременно навстречу друг другу отправились пешеход и велосипедист' \
    ' и встретились через ' + t.__str__() + ' ч. Определите скорость пешехода, если скорость велосипедиста равна ' + v.__str__() + ' км/ч.'

    logging.info('Ended generating motion problem with 1 - 3 difficulty level')
    return latex_string, v

def get_motion_problem_medium():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating motion problem with 4 - 6 difficulty level')
    v = random.randint(1, 10)
    u = random.randint(10, 30)
    t = random.randint(2, 6)
    h = random.randint(1, 4)
    s = u * h + (v + u) * t
    latex_string = 'Между двумя пунктами расстояние ' + s.__str__() + ' км, из первого пункта вышел пешеход, из второго спустя ' + h.__str__() + ' часов ' \
    'выехал велосипедист. Они встретились через ' + t.__str__() + ' ч. Определите скорость пешехода,' + 'если скорость велосипедиста равна ' + v.__str__() + ' км/ч.'

    logging.info('Ended generating motion problem with 4 - 6 difficulty level')
    return latex_string, v

def get_motion_problem_difficult():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating motion problem with 7 - 10 difficulty level')
    v = random.randint(5, 20)
    u = random.randint(5, 20)
    t = random.randint(2, 6)
    r = random.randint(1, 4)
    s = (v - r) * t + (u + r) * t
    latex_string = 'Две лодки одновременно начинают плыть от одного места на реке. Первая плывет против течения, вторая - по течению. ' \
                   'Определите рассточние между лодками через ' + t.__str__() + ' ч, если первая движется ' \
                    'со скоростью ' + v.__str__() + ' км/ч, а вторая - со скоростью ' +  u.__str__() + ' км/ч, скорость течения равна ' +  r.__str__() + ' км/ч.'

    logging.info('Ended generating motion problem with 7 - 10 difficulty level')
    return latex_string, s

def get_motion_problem_example():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started returning example of motion problem')

    example = get_motion_problem(0, 1)
    logging.info('Ended returning example of motion problem')
    return example