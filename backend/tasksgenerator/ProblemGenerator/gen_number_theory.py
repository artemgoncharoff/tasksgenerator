import logging
import random
from api import constants as c

# Возвращает уравнение одной неизвестно v1.0
def get_number_theory(seed, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating and returning LaTex string, correct answer, '
                 'answer template and header for number theory with difficulty level %s', difficulty)

    random.seed(seed)
    number_of_primes = 2
    if 3 <= difficulty and difficulty <= 7:
        number_of_primes = 3
    elif difficulty > 7:
        number_of_primes = 4

    correct_answer = []
    answer_template = []
    lower = difficulty // 2 + 2;
    upper = difficulty * 2 + 20;
    n = 1;
    for i in range(number_of_primes):
        a = random.randint(lower, upper)
        while is_not_prime(a):
            a = random.randint(lower, upper)
        correct_answer.append(a)
        answer_template.append('')
        n *= a

    header = 'Теория чисел. Разложение на простые множители'
    latex_string = 'Разложите число ' + n.__str__() + ' на простые множители. В ответ запишите числа в порядке неубывания.'

    logging.info('Ended generating and returning LaTex string, correct answer, '
                 'answer template and header for number theory with difficulty level %s', difficulty)
    return latex_string, correct_answer, answer_template, header

def get_number_theory_example():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started returning example of number theory')

    example = get_number_theory(0, 1)
    logging.info('Ended returning example of number theory')
    return example

def is_not_prime(number):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started detecting if %s is prime in number_theory', number)

    for i in range(2, number // 2 + 1):
        if number % i == 0:
            logging.info('Ended detecting if %s is prime in number theory', number)
            return True
    logging.info('Ended detecting if %s is prime in number theory', number)
    return False

