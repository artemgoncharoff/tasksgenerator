from . import get_expression as exp
from . import gen_linear_system as linear
from . import gen_number_theory as numth
from . import gen_meaning_expression as m_exp
from . import gen_motion_problem as mot
from . import gen_gcd_lcm as gcd_lcm

def get_tasks():
    tasks = [linear.get_linear_system, exp.get_expression, numth.get_number_theory, m_exp.get_meaning_expression, mot.get_motion_problem, gcd_lcm.get_gcd_lcm]
    return tasks

def get_examples():
    examples = [linear.get_linear_system_example, exp.get_expression_example, numth.get_number_theory_example, m_exp.get_meaning_expression_example, mot.get_motion_problem_example, gcd_lcm.get_gcd_lcm_example]
    return examples

tasks = get_tasks()
examples = get_examples()
