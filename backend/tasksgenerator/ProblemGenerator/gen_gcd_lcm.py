import random as rand

# Генерация НОКа
def get_lcm(diff):
    if diff < 7:
        a = 0
        b = 0
        lcm = 0
        while lcm == 0 or a == b:
            lcm = 0
            a = rand.randint(diff * 10, diff * 100)
            b = rand.randint(diff * 10, diff * 100)
            start = a if a > b else b
            for i in range(start, a * b):
                if i % a == 0 and i % b == 0:
                    lcm = i
                    break
        return a, b, lcm
    else:
        a = 0
        b = 0
        c = 0
        lcm = 0
        while lcm == 0 or a == b or b == c or a == c:
            lcm = 0
            a = rand.randint(10, diff * 50)
            b = rand.randint(10, diff * 50)
            c = rand.randint(10, diff * 50)
            min = a if a < b else b
            min = c if c < min else min
            for i in range(min, a * b * c):
                lcm = i
                if i % a == 0 and i % b == 0 and i % c == 0:
                    lcm = i
                    break
        return a, b, c, lcm

# Генерация НОДа
def get_gcd(diff):
    if diff < 7:
        a = 0
        b = 0
        gcd = 1
        while gcd < diff * 5 or a == b:
            a = rand.randint(diff * 10, diff * 50)
            b = rand.randint(diff * 10, diff * 50)
            min = a if a < b else b
            for i in range(2, min):
                if a % i == 0 and b % i == 0:
                    gcd = i

        return a, b, gcd
    else:
        a = 0
        b = 0
        c = 0
        gcd = 1
        while gcd < diff * 5 or a == b or b == c or a == c:
            a = rand.randint(diff, diff * 100)
            b = rand.randint(diff, diff * 100)
            c = rand.randint(diff, diff * 100)
            min = a if a < b else b
            min = c if c < min else min
            for i in range(2, min + 1):
                if a % i == 0 and b % i == 0 and c % i == 0:
                    gcd = i
        return a, b, c, gcd

# Генерация задания
def get_gcd_lcm(seed, difficulty):
    header = 'Нахождение НОК и НОД чисел'
    rand.seed(seed)
    if difficulty < 7:
        a, b, lcm = get_lcm(difficulty)
        latex_string = 'Найдите НОК для чисел ' + a.__str__() + ' и ' + b.__str__()
        a, b, gcd = get_gcd(difficulty)
        latex_string += ' и НОД для чисел ' + a.__str__() + ' и ' + b.__str__() + '.'
        correct_answer = [lcm, gcd]
        answer_template = ['НОК', 'НОД']
        return latex_string, correct_answer, answer_template, header
    else:
        a, b, c, lcm = get_lcm(difficulty)
        latex_string = 'Найдите НОК для чисел ' + a.__str__() + ', ' + b.__str__() + ' и ' + c.__str__()
        a, b, c, gcd = get_gcd(difficulty)
        latex_string += ' и НОД для чисел ' + a.__str__() + ', ' + b.__str__() + ' и ' + c.__str__() + '.'
        correct_answer = [lcm, gcd]
        answer_template = ['НОК', 'НОД']
        return latex_string, correct_answer, answer_template, header

# Возвращает пример задание (пример неизменяется)
def get_gcd_lcm_example():
    example = get_gcd_lcm(0, 1)
    return example


