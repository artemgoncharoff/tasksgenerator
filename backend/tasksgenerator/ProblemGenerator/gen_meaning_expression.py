import random

operators = ['+', '-', '\\cdot']


# Генерирует параметры
def gen_params(difficulty):
    params = []
    for i in range(difficulty + 1):
        params.append(random.randint(- difficulty * 10, difficulty * 10))
    return params


# Вспомогательный метод для генерации формулировки задачи
def gen_answer_string(params):
    s = ""
    for i in range(len(params)):
        s = s + 'x_{' + (i + 1).__str__() + '} = ' + params[i].__str__() + ', '

    return s


# Генерирует Latex строку, а также возвращает ответ на задание
def gen_latex_string(params):
    res = params[0]
    latex = 'x_1'
    for i in range(1, len(params)):
        ch = operators[random.randint(0, 2)]
        if ch == '+':
            latex = latex + ' + ' + 'x_{' + (i + 1).__str__() + '}'
            res += params[i]
        elif ch == '-':
            latex = latex + ' - ' + 'x_{' + (i + 1).__str__() + '}'
            res -= params[i]
        elif ch == '\\cdot':
            latex = '(' + latex + ')' + ' \\cdot ' + 'x_{' + (i + 1).__str__() + '}'
            res *= params[i]

    s = gen_answer_string(params)
    latex = "$" + latex + "\\text{ при }" + s + "$"
    return latex, res


def get_meaning_expression_example():
    return get_meaning_expression(0, 1)


def get_meaning_expression(seed, difficulty):
    header = 'Найти значение выражения'
    random.seed(seed)
    correct_answer = []
    params = gen_params(difficulty)
    latex_string, res = gen_latex_string(params)
    correct_answer.append(res)
    return latex_string, correct_answer, [""], header
