# coding=utf-8
import random
import logging


# Генерация ответов к СЛАУ
def get_correct_answer(difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating correct answers for linear system with difficulty level %s', difficulty)

    correct_answer = []
    for i in range(difficulty + 1):
        correct_answer.append(random.randint(-10, 10))

    logging.info('Ended generating correct answers for linear system with difficulty level %s', difficulty)
    return correct_answer


# Генерация матрицы СЛАУ
def get_matrix(variables, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started generating matrix with difficulty level %s and %s variables', difficulty, variables)

    matrix = []

    for i in range(difficulty + 1):
        result = 0
        matrix.append([])
        for j in range(difficulty + 1):
            x = random.randint(-10, 10)
            matrix[i].append(x)
            result = result + x * variables[j]
        matrix[i].append(result)

    logging.info('Ended generating matrix with difficulty level %s and %s variables', difficulty, variables)
    return matrix


# Возвращает представление СЛАУ в LaTeX при сложности <=6
def get_latex_string_low_diff(matrix):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started converting linear system to LaTex')

    tex_string = '\\begin{equation*} \n \\begin{cases} \n'

    for i in range(len(matrix[0]) - 1):
        for j in range(len(matrix[0]) - 1):
            if matrix[i][j] > 0 and matrix[i][j] != 1 and matrix[i][j] != -1:
                if j != 0:
                    tex_string += '+' + abs(matrix[i][j]).__str__() + 'x_{' + (j + 1).__str__() + '}'
                else:
                    tex_string += abs(matrix[i][j]).__str__() + 'x_{' + (j + 1).__str__() + '}'
            elif matrix[i][j] == 1:
                if j != 0:
                    tex_string += '+ x_{' + (j + 1).__str__() + '}'
                else:
                    tex_string += 'x_{' + (j + 1).__str__() + '}'
            elif matrix[i][j] == -1:
                tex_string += '-x_{' + (j + 1).__str__() + '}'
            elif matrix[i][j] < 0:
                tex_string += '-' + abs(matrix[i][j]).__str__() + 'x_{' + (j + 1).__str__() + '}'
        tex_string += '=' + matrix[i][-1].__str__() + '\n \\\\ \n'

    tex_string += '\end{cases} \n \end{equation*}'

    logging.info('Ended converting linear system to LaTex')
    return tex_string


# Возвращает матрицу элементов при сложности >6
def get_latex_string_high_diff(matrix):
    tex_string = '\\begin{pmatrix} \n'
    for i in range(len(matrix[0]) - 1):
        for j in range(len(matrix[0])):
            tex_string += matrix[i][j].__str__() + '&'
        tex_string += '\\' + '\\' + '\n'
    tex_string += '\\end{pmatrix}'
    return tex_string


# Возвращает массив имен полей ответа
def get_answer_template(difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting correct answers for linear system')

    answer_template = []
    for i in range(difficulty + 1):
        answer_template.append('$$x_{' + (i + 1).__str__() + '}$$')

    logging.info('Ended getting correct answers for linear system')
    return answer_template


# Возвращает пример задание (пример неизменяется)
def get_linear_system_example():
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting example of linear system')

    example = get_linear_system(0, 1)
    logging.info('Ended getting example of linear system')
    return example


# Возвращает латеховскую строку и массив с ответами
def get_linear_system(seed, difficulty):
    logging.basicConfig(filename='problem_generator.log', level=logging.DEBUG)
    logging.info('Started getting LaTex string, correct answer, answer template and header of linear system')

    header = 'Система линейных уравнений'
    random.seed(seed)
    correct_answer = get_correct_answer(difficulty)
    answer_template = get_answer_template(difficulty)
    matrix = get_matrix(correct_answer, difficulty)
    if difficulty <= 6:
        latex_string = get_latex_string_low_diff(matrix)
    else:
        latex_string = get_latex_string_high_diff(matrix)

    logging.info('Ended getting LaTex string, correct answer, answer template and header of linear system')
    return latex_string, correct_answer, answer_template, header
