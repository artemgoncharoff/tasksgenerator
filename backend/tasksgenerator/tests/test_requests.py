import unittest
from django.test import Client


class ProblemGeneratorTestCase(unittest.TestCase):
    def setUp(self):
        self.client = Client()

    def test_сonfig(self):
        response = self.client.get('/api/config')
        self.assertEqual(response.status_code, 200)

    def test_available_tasks(self):
        response = self.client.get('/api/available_tasks')
        self.assertEqual(response.status_code, 200)


