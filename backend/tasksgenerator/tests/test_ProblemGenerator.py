from django.test import TestCase
from ProblemGenerator import gen_number_theory as gnm
from ProblemGenerator import gen_linear_system as gls
from ProblemGenerator import get_expression as ge
from ProblemGenerator import gen_meaning_expression as gme
# import sys 
# sys.stderr.write(repr(gme.get_meaning_expression(i, i + 1)) + '\n')

class ProblemGeneratorTestCase(TestCase):

    # def test_gen_number_theory(self):
        # correct = []
        # correct.append(('Разложите число 3971 на простые множители. В ответ запишите числа в порядке неубывания.', [11, 19, 19], ['', '', ''], 'Теория чисел. Разложение на простые множители'))
        # correct.append(('Разложите число 385 на простые множители. В ответ запишите числа в порядке неубывания.', [5, 7, 11], ['', '', ''], 'Теория чисел. Разложение на простые множители'))
        # correct.append(('Разложите число 275 на простые множители. В ответ запишите числа в порядке неубывания.', [5, 5, 11], ['', '', ''], 'Теория чисел. Разложение на простые множители'))
        # correct.append(('Разложите число 105 на простые множители. В ответ запишите числа в порядке неубывания.', [3, 5, 7], ['', '', ''], 'Теория чисел. Разложение на простые множители'))
        # correct.append(('Разложите число 175 на простые множители. В ответ запишите числа в порядке неубывания.', [5, 5, 7], ['', '', ''], 'Теория чисел. Разложение на простые множители'))

        # self.assertEqual(len(gnm.get_number_theory(0, 1)), 4)
        # for i in range(5):
        # self.assertEqual(gnm.get_number_theory(i, i + 1), correct[i])
        # self.assertEqual(gnm.is_not_prime(1111), True)
        # self.assertEqual(gnm.is_not_prime(100), True)
        # self.assertEqual(gnm.is_not_prime(2), False)

    def test_gen_linear_system(self):
        self.assertEqual(len(gls.get_correct_answer(5)), 6)
        self.assertEqual(len(gls.get_answer_template(100)), 101)
        var = [1, 2, 3, 4]
        self.assertEqual(len(gls.get_matrix(var, 3)), 4)
        self.assertEqual(len(gls.get_matrix(var, 3)[0]), 5)
        self.assertEqual(len(gls.get_linear_system(0, 1)), 4)

        correct = [('\\begin{equation*} \n \\begin{cases} \n-9x_{1}-2x_{2}=-24\n \\\\ \n6x_{1}+5x_{2}=27\n \\\\ \n\\end{cases} \n \\end{equation*}', [2, 3], ['$$x_{1}$$', '$$x_{2}$$'], 'Система линейных уравнений'), ('\\begin{equation*} \n \\begin{cases} \n-2x_{1}-7x_{2}+5x_{3}=-84\n \\\\ \n4x_{1}+5x_{2}+10x_{3}=-64\n \\\\ \n2x_{1}-4x_{2}-7x_{3}=12\n \\\\ \n\\end{cases} \n \\end{equation*}', [-6, 8, -8], ['$$x_{1}$$', '$$x_{2}$$', '$$x_{3}$$'], 'Система линейных уравнений'), ('\\begin{equation*} \n \\begin{cases} \n-5x_{1}-x_{2}-2x_{3}+9x_{4}=78\n \\\\ \n-4x_{1}+9x_{2}-9x_{3}+8x_{4}=44\n \\\\ \n-5x_{1}+3x_{2}+10x_{3}+2x_{4}=-57\n \\\\ \n6x_{1}+ x_{2}+7x_{3}+4x_{4}=-114\n \\\\ \n\\end{cases} \n \\end{equation*}', [-9, -8, -8, 1], ['$$x_{1}$$', '$$x_{2}$$', '$$x_{3}$$', '$$x_{4}$$'], 'Система линейных уравнений'), ('\\begin{equation*} \n \\begin{cases} \n9x_{1}+5x_{2}+10x_{3}+8x_{4}-8x_{5}=27\n \\\\ \n9x_{1}-10x_{2}+5x_{3}-2x_{4}+7x_{5}=-53\n \\\\ \n-3x_{1}-4x_{2}+5x_{3}+7x_{4}+7x_{5}=-23\n \\\\ \n5x_{1}+2x_{2}+10x_{3}-6x_{4}-3x_{5}=104\n \\\\ \n10x_{1}-6x_{2}+6x_{3}+2x_{4}-10x_{5}=-58\n \\\\ \n\\end{cases} \n \\end{equation*}', [-3, 8, 7, -6, 1], ['$$x_{1}$$', '$$x_{2}$$', '$$x_{3}$$', '$$x_{4}$$', '$$x_{5}$$'], 'Система линейных уравнений'), ('\\begin{equation*} \n \\begin{cases} \n-8x_{1}-8x_{2}-10x_{3}+2x_{4}+7x_{5}-x_{6}=147\n \\\\ \n-9x_{1}-3x_{2}+6x_{3}+7x_{4}+ x_{5}-2x_{6}=19\n \\\\ \n-5x_{1}-7x_{2}-2x_{3}-4x_{4}-10x_{5}+10x_{6}=-82\n \\\\ \n-2x_{1}-2x_{2}-4x_{3}-5x_{4}-x_{5}-x_{6}=27\n \\\\ \n10x_{1}+ x_{2}-8x_{3}+9x_{4}+2x_{6}=31\n \\\\ \n6x_{1}-3x_{2}-5x_{3}-3x_{4}+5x_{5}-2x_{6}=51\n \\\\ \n\\end{cases} \n \\end{equation*}', [-3, -1, -7, 2, 5, -6], ['$$x_{1}$$', '$$x_{2}$$', '$$x_{3}$$', '$$x_{4}$$', '$$x_{5}$$', '$$x_{6}$$'], 'Система линейных уравнений')]

        for i in range(5):
            self.assertEqual(gls.get_linear_system(i, i + 1), correct[i])

    def test_get_expression(self):
        self.assertEqual(len(ge.get_correct_answer()), 1)
        self.assertEqual(len(ge.get_answer_template()), 1)
        correct = [('$x/ \\left( -9\\right) = -2/9$', [2], ['$$x$$'], 'Вычисление значения выражения'), ('$\\left( x-2\\right) / 4= -2$', [-6], ['$$x$$'], 'Вычисление значения выражения'),('$\\left| x-8\\right| \\cdot \\left( -2\\right) = -34, x \\leq 0$', [-9], ['$$x$$'], 'Вычисление значения выражения'),('$\\left( \\left| x^{-2}\\cdot 9\\right| \\right) ^{-3}= 1, x \\leq 0$', [-3], ['$$x$$'], 'Вычисление значения выражения'),('$\\left( \\left| x\\cdot \\left( -7\\right) \\right| / 5+8\\right) / 7= 61/35, x \\leq 0$', [-3], ['$$x$$'], 'Вычисление значения выражения')]
        for i in range(5):
            self.assertEqual(ge.get_expression(i, i + 1), correct[i])

    def test_get_expression(self):
        correct = [('$x_1 + x_{2}\\text{ при }x_{1} = 2, x_{2} = 3, $', [5], [''], 'Найти значение выражения'), ('$x_1 - x_{2} + x_{3}\\text{ при }x_{1} = -12, x_{2} = 16, x_{3} = -16, $', [-44], [''], 'Найти значение выражения'), ('$x_1 + x_{2} + x_{3} - x_{4}\\text{ при }x_{1} = 25, x_{2} = 30, x_{3} = 24, x_{4} = -27, $', [106], [''], 'Найти значение выражения'), ('$(((x_1) \\cdot x_{2} - x_{3}) \\cdot x_{4}) \\cdot x_{5}\\text{ при }x_{1} = -10, x_{2} = 35, x_{3} = 29, x_{4} = -24, x_{5} = 7, $', [63672], [''], 'Найти значение выражения'), ('$x_1 + x_{2} + x_{3} + x_{4} + x_{5} - x_{6}\\text{ при }x_{1} = -20, x_{2} = -12, x_{3} = -37, x_{4} = 42, x_{5} = 0, x_{6} = 11, $', [-38], [''], 'Найти значение выражения')]
        for i in range(5):
            self.assertEqual(gme.get_meaning_expression(i, i + 1), correct[i])




