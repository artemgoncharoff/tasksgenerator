echo "Create eb profile"
rm -r ~/.aws
mkdir ~/.aws
touch ~/.aws/credentials
printf "[eb-cli]\naws_access_key_id = %s\naws_secret_access_key = %s\n" "$CI_AWS_EB_KEY_ID" "$CI_AWS_EB_KEY_SECRET" >> ~/.aws/credentials
touch ~/.aws/config
printf "[profile eb-cli]\nregion=us-east-1\noutput=json" >> ~/.aws/config
echo "eb init"
eb init --region us-east-1 --platform "Multi-container Docker" Tasksgenerator
echo "eb deploy"
eb deploy Tasksgenerator-env
