#git add .
#git commit -m "somemsg"
docker logout
echo "$CI_DOCKER_REGISTRY_PASSWORD" | docker login --password-stdin -u "$CI_DOCKER_REGISTRY_USER" $CI_DOCKER_REGISTRY
docker build -t belkin/tasksgenerator_nginx:latest .
docker build -t belkin/tasksgenerator_back:latest backend/tasksgenerator
docker build -t belkin/tasksgenerator_front:latest frontend/tasksgenerator
docker push belkin/tasksgenerator_nginx:latest
docker push belkin/tasksgenerator_back:latest
docker push belkin/tasksgenerator_front:latest
